import crypto from 'crypto';
import fs from 'fs';

const resource = 'jquery-3.6.4.min.js';
const expectedIntegrity = 'oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=';

const jquerySource = fs.readFileSync(resource);
const digest = crypto
  .createHash('sha256')
  .update(jquerySource, 'utf8')
  .digest();
const calculatedIntegrity = digest.toString('base64');

if (calculatedIntegrity == expectedIntegrity) {
  console.log('✔️ Resource integrity verified!');
} else {
  console.log(`❌ Failed to find a valid digest in the 'integrity' attribute 
  for resource '${resource}' with computed SHA-256 integrity '${calculatedIntegrity}'.`);
}
